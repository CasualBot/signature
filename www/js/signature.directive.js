'use strict';

angular.module('starter')
.directive('signature', signature);

function signature() {
  return {
    restrict: 'E',
    templateUrl: 'views/signature.html',
    controller: 'signatureController',
  }
}