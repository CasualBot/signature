'use strict';

angular.module('starter')
.controller('beaconController', beaconController);


function beaconController(
    $scope
) {
  // Request authorisation.
  estimote.beacons.requestAlwaysAuthorization();

  // Start ranging.
  estimote.beacons.startRangingBeaconsInRegion({}, onRange, onError);

  angular.extend($scope, {
      getBeacons: getBeacons,
  });

  function calculateProx(prox) {
    switch(prox) {
      case 0:
        return "Unknown";
        break;
      case 1:
        return "Close by";
        break;
      case 2:
        return "Within walking distance";
        break;
      case 3:
        return "Far away";
        break;
      default:
        return "Unknown";
    }
  }

  function displayBeconInfo(beaconInfo) {
    console.log(beaconInfo);

    // Clear beacon HTML items.
    $('#id-screen-range-beacons .style-item-list').empty();
    // Sort beacons by distance.
    beaconInfo.beacons.sort(function (beacon1, beacon2) {
      return beacon1.distance > beacon2.distance;
    });

    // Generate HTML for beacons.
    $.each(beaconInfo.beacons, function (key, beacon) {
      var element = $(createBeaconHTML(beacon));
      $('#id-screen-range-beacons .style-item-list').append(element);
    });

  }

  function onRange(beaconInfo) {
    displayBeconInfo(beaconInfo);
  }

  function onError(errorMessage) {
    console.log('Range error: ' + errorMessage);
  }

  function createBeaconHTML(beacon) {
    console.log(beacon)
    var colorClasses = beacon.color;
    var htm = '<div class="' + colorClasses + '">'
        + '<table><tr><td>Major: </td><td>' + beacon.major
        + '</td></tr><tr><td>Minor: </td><td>' + beacon.minor
        + '</td></tr><tr><td>RSSI: </td><td>' + beacon.rssi
        + '</td></tr><tr><td>Name: </td><td>' + beacon.name
    if (beacon.proximity) {
      htm += '</td></tr><tr><td>Proximity: </td><td>'
          + calculateProx(beacon.proximity)
    }
    if (beacon.distance) {
      htm += '</td></tr><tr><td>Distance</td><td>'
          + formatDistance(beacon.distance)
    }
    htm += '</td></tr></table></div>';
    return htm;
  }

  function formatDistance(meters) {
    if (!meters) { return 'Unknown'; }

    if (meters > 1)
    {
      return meters.toFixed(3) + ' m';
    }
    else
    {
      return (meters * 100).toFixed(3) + ' cm';
    }
  }

}