'use strict';

angular.module('starter')
  .controller('signatureController', signatureController);


function signatureController(
    $scope
) {


  var canvas = document.querySelector("canvas");
  var signaturePad = new SignaturePad(canvas);
  signaturePad.minWidth = 2;
  signaturePad.maxWidth = 3;
  signaturePad.penColor = "rgb(0,0,0)";


  angular.extend($scope, {
    signaturePad: signaturePad,
    save: saveSignature,
    imgSrc:'',
  });

  function saveSignature() {
    $scope.imgSrc = _.trim(signaturePad.toDataURL(),'data:image/png;base64');
  }

}