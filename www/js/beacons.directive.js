'use strict';

angular.module('starter')
    .directive('beacons', beacons);

function beacons() {
  return {
    restrict: 'E',
    templateUrl: 'views/beacons.html',
    controller: 'beaconController',
  }
}